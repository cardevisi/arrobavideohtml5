(function(){
	var stepA = false;
	var clickTag = 'http://www.google.com.br;';
	var endVideo = false;
	
	function htmlVideo(callback) {
		var div = document.createElement('div');
		div.id = 'video-stepA';
		div.style.width = '300px';
		div.style.height = '250px';
		div.style.position = 'absolute';
		div.style.top = 0;
		div.style.left = 0;
		div.style.backgroundColor = '#000';
		var video = document.createElement('video');
		video.id = 'uol-insercao-html-video';
		video.src = window.UOLI.videoBannerHTML5.urlVideo;
		video.setAttribute('display', 'block');
		video.setAttribute('width', 300);
		video.setAttribute('height', 250);
		video.setAttribute('cursor', 'pointer');
		video.setAttribute('controls', '');
		video.setAttribute('autoplay', '');
		video.style.position = 'absolute';
		video.style.top = '0';
		video.style.left = '0';
		video.style.zIndex = 99999999;
		video.addEventListener('pause', function(){
			console.log('pause');
		});
		video.addEventListener('play', function(){
			console.log('play');
		});
		video.addEventListener('volume', function(){
			console.log('play');
		});
		video.play();
		
		video.addEventListener('ended', function(){
			callback();
		});
		div.appendChild(video);
		var container = document.getElementById('tm-container-stepA');
		container.appendChild(div);
	};

	window.UOLI.videoBannerHTML5.htmlVideoHide = function() {
		var div = document.getElementById('uol-insercao-html-video');
		div.style.display = 'none';
	}

	function hideStepA() {
		var div = document.getElementById('img-stepA');
		div.style.display = 'none';
	}

	function getClickTag() {
		var clickTag = (window.UOLI.videoBannerHTML5.clickTagValue) ? window.UOLI.videoBannerHTML5.clickTagValue : '%%CLICK_URL_UNESC%%%%DEST_URL%%';
		return clickTag;
	}

	function frameA (container) {
		var div = document.createElement('div');
		div.id = 'tm-container-stepA';
		div.style.width = '300px';
		div.style.height = '250px';
		div.style.position = 'relative';
		div.addEventListener('click', function(){
			if (!stepA) {
				if(htmlVideo()) {
					endVideo = true;
				}
				stepA = true;
			} 

			if(endVideo) {
				getClickTag();
			}
		});
		var img = document.createElement('img');
		img.id = 'img-stepA';
		img.src = '300x250.gif';
		div.appendChild(img);
		container.appendChild(div);
	}

	function init() {
		var container = document.getElementById(window.UOLI.videoBannerHTML5.targetId);
		if (container === null) {
			return;
		}
		frameA(container);
	}
	
	window.UOLI = window.UOLI || {};
	window.UOLI.videoBannerHTML5 = window.UOLI.videoBannerHTML5 || {};
	window.UOLI.videoBannerHTML5.urlVideo = 'http://bn.imguol.com/1505/secom/20/longo.mp4';
	window.UOLI.videoBannerHTML5.clickTagValue = '';
	window.UOLI.videoBannerHTML5.targetId = 'banner-300x250-3_ad_container';

	init();
})();