var canvas, stage, exportRoot;
function handleFileLoad(evt) {
	if (evt.item.type == "image") { images[evt.item.id] = evt.result; }
}

function htmlVideo(callback) {
	var video = document.createElement('video');
	video.id = 'uol-insercao-html-video';
	video.src = 'longo.mp4';
	video.width = 276;
	video.heigt = 230;
	video.style.position = 'absolute';
	video.style.top = '22px';
	video.style.left = '13px';
	video.style.zInde = 99999999;
	video.play();
	video.addEventListener('ended', function(){
		callback();
	});
	var container = document.getElementById('tm-container-html5-canvas');
	container.appendChild(video);
}

function htmlVideoHide() {
	var div = document.getElementById('uol-insercao-html-video');
	div.style.display = 'none';
}

function handleComplete() {
	exportRoot = new lib.html5final();

	stage = new createjs.Stage(canvas);
	stage.addChild(exportRoot);
	stage.update();
	stage.enableMouseOver();

	createjs.Ticker.setFPS(lib.properties.fps);
	createjs.Ticker.addEventListener("tick", stage);
}

function cretaContainers (container) {
	var div = document.createElement('div');
	div.id = 'tm-container-html5-canvas';
	div.style.width = '300px';
	div.style.height = '250px';
	div.style.position = 'relative';
	var canvas = document.createElement('canvas');
	canvas.id = "canvas";
	canvas.width = 300;
	canvas.height = 250;
	canvas.style.backgroundColor = '#FFFFFF';
	div.appendChild(canvas);
	container.appendChild(div);
}

function getCanvasElement() {
	canvas = document.getElementById("canvas");
	images = images||{};

	var loader = new createjs.LoadQueue(false);
	loader.addEventListener("fileload", handleFileLoad);
	loader.addEventListener("complete", handleComplete);
	loader.loadManifest(lib.properties.manifest);
}

function init() {
	var container = document.getElementById('banner-300x250');
	cretaContainers(container);
	getCanvasElement();
}

init();