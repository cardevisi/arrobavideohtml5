(function(){
	
	window.UOLI = window.UOLI || {};
	window.UOLI.videoBannerHTML5 = window.UOLI.videoBannerHTML5 || {};
	window.UOLI.videoBannerHTML5.urlVideo = 'http://bn.imguol.com/1505/secom/20/longo.mp4';
	window.UOLI.videoBannerHTML5.pathImgFrameA = 'frameA.gif';
	window.UOLI.videoBannerHTML5.pathImgFrameB = 'frameB.gif';
	window.UOLI.videoBannerHTML5.clickTagValue = '<insert url>';
	
	var stepA = false;
	var endVideo = false;
	var read = {};
	var track = {
		'unmute':'<insert key>',
		'mute'	:'<insert key>',
		'play'	:'<insert key>',
		'pause' :'<insert key>',
		'end'   :'<insert key>',
		'25'	:'<insert key>',
		'50'	:'<insert key>',
		'75'	:'<insert key>',
		'100'	:'<insert key>'
	};

	function htmlVideo(callback) {
		var div = document.createElement('div');
		div.id = 'video-stepA';
		div.style.width = '300px';
		div.style.height = '250px';
		div.style.position = 'absolute';
		div.style.top = 0;
		div.style.left = 0;
		div.style.backgroundColor = '#000';
		var video = document.createElement('video');
		video.id = 'uol-insercao-html-video';
		video.src = window.UOLI.videoBannerHTML5.urlVideo;
		video.setAttribute('display', 'block');
		video.setAttribute('width', 300);
		video.setAttribute('height', 250);
		video.setAttribute('cursor', 'pointer');
		video.setAttribute('controls', '');
		video.setAttribute('autoplay', '');
		
		video.style.position = 'absolute';
		video.style.top = '0';
		video.style.left = '0';
		video.style.zIndex = 99999999;

		video.addEventListener('click', function(){
			getClickTag();
		});
		
		video.addEventListener('pause', function(){
			trackEvents(track['pause'],'pause');
		});

		video.addEventListener('play', function(){
			trackEvents(track['play'],'play');
		});

		video.addEventListener('volumechange', function(e){
			var video = document.getElementById("uol-insercao-html-video");
			if(video.muted) {
				video.muted= true;
				trackEvents(track['mute'],'mute');
			} else {
				video.muted= false;
				trackEvents(track['unmute'],'unmute');
			}
		});

		video.addEventListener('ended', function(){
			trackEvents(track['end'],'end');
			read = {};
			hideVideoStepA();
			frameB();
			callback();
		});

		video.addEventListener('timeupdate', function(){

			var percent =  Math.floor((this.currentTime * 100) / this.duration);
			
			if (percent === 25 && !read['25']) {
				trackEvents(track['25'], '25');
				read['25'] = true;
			}
			else if (percent === 50 && !read['50']) {
				trackEvents(track['50'], '50');
				read['50'] = true;
			}
			else if (percent === 75 && !read['75']) {
				trackEvents(track['75'], '75');
				read['75'] = true;
			}
			else if (percent === 100 && !read['100']) {
				trackEvents(track['100'], '100');
				read['100'] = true;
			}
	
		});

		video.play();
		
		div.appendChild(video);
		var container = document.getElementById('tm-container-stepA');
		container.appendChild(div);
	};

	function hideVideoStepA() {
		var div = document.getElementById('video-stepA');
		div.style.display = 'none';
	};

	function trackEvents (id, params) {
		var imgTracker = new Image();
    	imgTracker.src = 'http://www5.smartadserver.com/call/pubimppixel/'+ id +'/'+ (new Date()).getTime() +'?'+ params;
	};

	function hideStepA() {
		var div = document.getElementById('img-stepA');
		div.style.display = 'none';
	};

	function getClickTag() {
		var clickTag = (window.UOLI.videoBannerHTML5.clickTagValue) ? window.UOLI.videoBannerHTML5.clickTagValue : '%%CLICK_URL_UNESC%%%%DEST_URL%%';
		window.location = clickTag;
	};

	function frameA (container) {
		var div = document.createElement('div');
		div.id = 'tm-container-stepA';
		div.style.width = '300px';
		div.style.height = '250px';
		div.style.position = 'relative';
		div.addEventListener('click', clickEvent);
		var img = document.createElement('img');
		img.id = 'img-stepA';
		img.src = window.UOLI.videoBannerHTML5.pathImgFrameA;
		div.appendChild(img);
		container.appendChild(div);
	};

	function frameB(){
		var img = document.getElementById('img-stepA');
		img.src = window.UOLI.videoBannerHTML5.pathImgFrameB;
	};

	function clickEvent() {
		if (!stepA) {
			htmlVideo(function(){
				endVideo = true;
			});
			stepA = true;
		} 

		if(endVideo) {
			getClickTag();
		}
	};

	function init() {
		var container = document.currentScript.parentNode;
		if (container === null) {
			return;
		}
		frameA(container);
	};
	
	init();
})();